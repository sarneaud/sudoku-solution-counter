# Monte-Carlo-based Sudoku Solutions Calculator
Program that demonstrates estimating the number of possible sudoku solution grids using a Monte Carlo technique.

It works by randomly generating sudoku puzzles and then generating all possible solutions to them.  Using a technique described by Donald Knuth in the 1970s, this can be used to statistically estimate the total number of solutions to a blank grid.

A [more detailed explanation](https://theartofmachinery.com/2017/08/14/monte_carlo_counting_sudoku_grids.html) is on my blog.

## Usage
Compile and run in a console ([`ldc`](https://github.com/ldc-developers/ldc/) is recommended for performance).  The program will keep generating estimates, and the average of these estimates will converge to the correct value.

Example of generating 100 estimates:
```
./sudoku_solutions | head 100
```

There are some parameters that can be tweaked at the top of the source file.
