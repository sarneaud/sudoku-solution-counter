import std.algorithm;
import std.array;
import std.bigint;
import std.random;
import std.range;
import io = std.stdio;
import std.utf;

// 3 for a normal 9x9 sudoku, 2 for a simplified 4x4 version
enum kBoxLength = 3;
// Number of squares to fill randomly before letting a backtracking search take over
// A smaller number increases estimate accuracy, but greatly increases search time
enum kRandomSearchDepth = 25;

enum kGridLength = kBoxLength * kBoxLength;
static assert (kRandomSearchDepth <= kGridLength * kGridLength);

void main()
{
	auto rng = Random(unpredictableSeed());
	while(true)
	{
		auto estimate = estimateSolutionCount(rng);
		io.writeln(estimate);
		io.stdout.flush();
	}
}

BigInt estimateSolutionCount(Rng)(ref Rng rng) pure
{
	Grid grid = Grid();
	auto squares = genAllCoords();
	squares.partialShuffle(kRandomSearchDepth, rng);
	auto estimate = grid.monteCarloFill(rng, squares[0..kRandomSearchDepth]);
	debug
	{
		io.writeln(grid);
	}
	if (estimate != 0) estimate *= grid.numSolutions(squares[kRandomSearchDepth..$]);
	return estimate;
}

BigInt monteCarloFill(Rng)(ref Grid grid, ref Rng rng, Coord[] squares) pure
{
	BigInt branching_factor = 1;
	foreach (pos; squares)
	{
		auto choices = grid.availableDigits(pos);
		branching_factor *= choices.length;
		if (choices.empty) break;
		grid.set(pos, choices.getRandom(rng));
	}
	return branching_factor;
}

unittest
{
	auto rng = Xorshift128(1);
	auto grid = Grid();
	auto squares = genAllCoords();
	squares.partialShuffle(kRandomSearchDepth, rng);
	auto branching_factor = grid.monteCarloFill(rng, squares[0..kRandomSearchDepth]);

	assert (branching_factor > 0);
	foreach (pos; squares[0..kRandomSearchDepth])
	{
		assert (grid[pos] != 0);
	}
	assert (squares.map!(pos => grid[pos]).filter!(digit => digit != 0).walkLength == kRandomSearchDepth);
}

long numSolutions(ref Grid grid, Coord[] squares) pure
in
{
	assert (squares.length + grid.num_filled == kGridLength * kGridLength);
}
body
{
	// Simple backtracking solver

	if (squares.empty)
	{
		assert (grid.isComplete());
		return 1L;
	}

	long total = 0;
	auto pos = squares[0];
	auto choices = grid.availableDigits(pos);
	foreach (d; choices[])
	{
		auto saved_state = grid.set(pos, d);
		total += numSolutions(grid, squares[1..$]);
		grid.restore(saved_state);
	}
	return total;
}

unittest
{
	static if (kBoxLength == 3)
	{
		struct TestCase
		{
			uint seed;
			size_t num_digits;
			long answer;
		}

		// Test data cross-checked against a sudoku solver implemented with answer set programming (ASP)
		auto cases = [
			TestCase(4078865956, 30, 0),
			// +---+---+---+
			// |...|..3|942|
			// |6..|.7.|...|
			// |..4|..1|.5.|
			// +---+---+---+
			// |1.9|.6.|.2.|
			// |8.2|..9|...|
			// |.35|.2.|.8.|
			// +---+---+---+
			// |2.3|...|891|
			// |...|2..|.35|
			// |9..|7..|...|
			// +---+---+---+

			TestCase(4078865956, 25, 257),
			// +---+---+---+
			// |...|..6|17.|
			// |6..|.3.|...|
			// |..5|..8|.9.|
			// +---+---+---+
			// |3.2|.1.|...|
			// |7..|...|...|
			// |.94|.5.|.6.|
			// +---+---+---+
			// |4..|...|928|
			// |...|4..|.53|
			// |5..|3..|...|
			// +---+---+---+

			TestCase(4078865959, 27, 154),
			// +---+---+---+
			// |2.4|...|...|
			// |...|1..|...|
			// |6.9|.2.|.7.|
			// +---+---+---+
			// |56.|..1|..7|
			// |4.7|.52|...|
			// |...|..7|..2|
			// +---+---+---+
			// |...|.1.|..3|
			// |17.|.48|6.9|
			// |...|2..|.5.|
			// +---+---+---+

			TestCase(4078865989, 30, 3),
			// +---+---+---+
			// |...|...|...|
			// |634|57.|2.9|
			// |...|3..|...|
			// +---+---+---+
			// |..1|..4|..5|
			// |...|..3|.78|
			// |2..|75.|.1.|
			// +---+---+---+
			// |4..|18.|7..|
			// |5.2|4.7|6..|
			// |..3|9..|.5.|
			// +---+---+---+
		];

		foreach (test_case; cases)
		{
			auto rng = Xorshift128(test_case.seed);
			auto grid = Grid();
			auto squares = genAllCoords();
			squares.partialShuffle(test_case.num_digits, rng);
			grid.monteCarloFill(rng, squares[0..test_case.num_digits]);
			assert (grid.numSolutions(squares[test_case.num_digits..$]) == test_case.answer);
		}
	}
}

struct Coord
{
	ushort row, col;

	size_t boxNumber() const pure
	{
		return kBoxLength * (row / kBoxLength) + col / kBoxLength;
	}

	invariant
	{
		assert (row < kGridLength);
		assert (col < kGridLength);
	}
}

Coord[] genAllCoords() pure
{
	auto positions = iota!(ushort, ushort)(0, kGridLength);
	return cartesianProduct(positions, positions).map!(p => Coord(p[0], p[1])).array;
}

unittest
{
	auto all_coords = genAllCoords().array;
	assert (all_coords.length == kGridLength * kGridLength);
	assert (all_coords.sort.uniq.walkLength == kGridLength * kGridLength);

	auto box_histogram = all_coords.map!(c => c.boxNumber()).array.sort.group.array;
	assert (box_histogram.length == kGridLength);
	assert (equal(box_histogram.map!(t => t[0]), iota(0, kGridLength))); // box numbers
	assert (equal(box_histogram.map!(t => t[1]), repeat(kGridLength, kGridLength))); // box number counts
}

struct Grid
{
	size_t num_filled() const pure
	{
		return _num_filled;
	}

	bool isComplete() const pure
	{
		return _num_filled == kGridLength * kGridLength;
	}

	ChoiceList availableDigits(Coord pos) const pure
	{
		auto box_num = pos.boxNumber();
		return _remaining_in_row[pos.row] & _remaining_in_col[pos.col] & _remaining_in_box[box_num];
	}

	int opIndex(Coord pos) const pure
	{
		return _squares[pos.row][pos.col];
	}

	SaveState set(Coord pos, int digit) pure
	in
	{
		assert (digit >= 1 && digit <= kGridLength);
		assert (availableDigits(pos).hasChoice(digit));
		assert (_squares[pos.row][pos.col] == 0);
	}
	out
	{
		assert (this[pos] == digit);
	}
	body
	{
		auto box_num = pos.boxNumber();
		auto saved_state = SaveState(pos, _remaining_in_row[pos.row], _remaining_in_col[pos.col], _remaining_in_box[box_num]);

		_squares[pos.row][pos.col] = digit;
		_num_filled++;
		_remaining_in_row[pos.row] = _remaining_in_row[pos.row].without(digit);
		_remaining_in_col[pos.col] = _remaining_in_col[pos.col].without(digit);
		_remaining_in_box[box_num] = _remaining_in_box[box_num].without(digit);

		return saved_state;
	}

	void restore(SaveState saved_state) pure
	{
		auto pos = saved_state.pos;
		_remaining_in_row[pos.row] = saved_state.row_choices;
		_remaining_in_col[pos.col] = saved_state.col_choices;
		_remaining_in_box[pos.boxNumber()] = saved_state.box_choices;
		_squares[pos.row][pos.col] = 0;
		_num_filled--;
	}

	void toString(scope void delegate(const(char)[]) sink) const
	{
		void writeRow(size_t row_num)
		{
			char[kGridLength] buffer;
			foreach (idx, d; _squares[row_num])
			{
				if (d == 0)
				{
					buffer[idx] = '.';
				}
				else
				{
					buffer[idx] = cast(char)('0' + d);
				}
			}
			sink("|");
			foreach (j; 0..kBoxLength)
			{
				sink(buffer[kBoxLength*j..kBoxLength*(j+1)]);
				sink("|");
			}
			sink("\n");
		}

		char[kGridLength + kBoxLength + 1] horizontal_rule;
		horizontal_rule[0] = horizontal_rule[$-1] = '+';
		foreach (idx, char c; '-'.repeat(kBoxLength).repeat(kBoxLength).joiner("+".byChar).enumerate(1))
		{
			// std.algorithm.copy breaks thanks to autodecoding
			horizontal_rule[idx] = c;
		}

		size_t row_num = 0;
		sink(horizontal_rule[]);
		sink("\n");
		foreach (j; 0..kBoxLength)
		{
			foreach (k; 0..kBoxLength)
			{
				writeRow(row_num++);
			}
			sink(horizontal_rule[]);
			sink("\n");
		}
	}

	private:

	struct SaveState
	{
		Coord pos;
		ChoiceList row_choices, col_choices, box_choices;
	}

	int[kGridLength][kGridLength] _squares;
	size_t _num_filled = 0;
	ChoiceList[kGridLength] _remaining_in_row, _remaining_in_col, _remaining_in_box;
}

struct ChoiceList
{
	// A list of available choices is represented with a bitfield, with 1s representing available choices
	import core.bitop;

	size_t length() const pure
	{
		return _popcnt(_choice_flags);
	}

	unittest
	{
		assert (ChoiceList(0b000).length == 0);
		assert (ChoiceList(0b101).length == 2);
		assert (ChoiceList(0b111111111).length == 9);
	}

	bool empty() const pure
	{
		return length == 0;
	}

	bool hasChoice(int choice_idx) const pure
	in
	{
		assert (choice_idx >= 1 && choice_idx <= 10);
	}
	body
	{
		return (_choice_flags & (1 << (choice_idx - 1))) != 0;
	}

	ChoiceList without(int choice_idx) const pure
	in
	{
		assert (choice_idx >= 1 && choice_idx <= 10);
	}
	out (result)
	{
		assert (!result.hasChoice(choice_idx));
	}
	body
	{
		return ChoiceList(_choice_flags & ~(1 << (choice_idx - 1)));
	}

	unittest
	{
		assert (ChoiceList(0b101).without(1) == ChoiceList(0b100));
		assert (ChoiceList(0b101).without(3) == ChoiceList(0b001));
		assert (ChoiceList(0b111111111).without(4) == ChoiceList(0b111110111));
		assert (ChoiceList(0b111111111).without(9) == ChoiceList(0b011111111));
	}

	int getRandom(Rng)(ref Rng rng) const pure
	in
	{
		assert (!empty);
	}
	out (result)
	{
		assert (hasChoice(result));
	}
	body
	{
		auto selected_idx = uniform(0, length, rng), idx = 0;
		foreach (v; this)
		{
			if (idx++ == selected_idx) return v;
		}
		assert (0);
	}

	ChoiceList opBinary(string op)(const(ChoiceList) rhs) const pure if (op == "&")
	{
		return ChoiceList(_choice_flags & rhs._choice_flags);
	}

	unittest
	{
		assert ((ChoiceList(0b101) & ChoiceList(0b011)) == ChoiceList(0b001));
		assert ((ChoiceList(0b011) & ChoiceList(0b101)) == ChoiceList(0b001));
		assert ((ChoiceList(0b10) & ChoiceList(0b01)) == ChoiceList(0b00));
		assert ((ChoiceList(0b0) & ChoiceList(0b0)) == ChoiceList(0b0));
		assert ((ChoiceList(0b1) & ChoiceList(0b1)) == ChoiceList(0b1));
	}

	Range opSlice() const pure
	{
		return Range(_choice_flags);
	}

	struct Range
	{
		bool empty() const pure
		{
			return _bits == 0;
		}

		int front() const pure
		{
			version (LDC)
			{
				// LDC doesn't inline this otherwise
				import ldc.intrinsics;
				return llvm_cttz(_bits, true) + 1;
			}
			else
			{
				return bsf(_bits) + 1;
			}
		}

		void popFront() pure
		{
			_bits &= _bits - 1;
		}

		private:
		ushort _bits;
	}

	unittest
	{
		import std.typecons : tuple;

		auto cases = [
			tuple(0b000, (int[]).init),
			tuple(0b001, [1]),
			tuple(0b010, [2]),
			tuple(0b100, [3]),
			tuple(0b101, [1, 3]),
			tuple(0b111111111, [1, 2, 3, 4, 5, 6, 7, 8, 9]),
		];

		foreach (test_case; cases)
		{
			int[] result;
			foreach (v; ChoiceList(cast(ushort)test_case[0])[])
			{
				result ~= v;
			}

			assert (equal(result, test_case[1]));
		}
	}

	private:

	ushort _choice_flags = (1 << kGridLength) - 1;
}
